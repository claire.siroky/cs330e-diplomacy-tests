#!/usr/bin/env python3

# -------------------------------
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):

    # ----
    # solve
    # ----

    def test_solve_1(self):
        list1in = ["A Madrid Hold","B London Support A","C Austin Move London","D Rome Hold","E Barcelona Move Madrid"]
        list1out = ["A [dead]","B [dead]","C [dead]","D Rome","E [dead]"]
        v = diplomacy_solve(list1in)
        self.assertEqual(v,list1out)

    def test_solve_2(self):
        list2in = ["A Paris Hold","B Madrid Support A","C London Move Madrid","D Warsaw Support C","E Berlin Support B","F Nice Support C"]
        list2out = ["A Paris","B [dead]","C Madrid","D Warsaw","E Berlin","F Nice"]
        v = diplomacy_solve(list2in)
        self.assertEqual(v,list2out)

    def test_solve_3(self):
        list3in = ["A Paris Hold","B Madrid Support A","C London Support B","D Warsaw Support C","E Berlin Support D","F Nice Support E","G Amsterdam Move Madrid","H Oslo Move Paris","I Moscow Support H"]
        list3out = ["A [dead]","B Madrid","C London","D Warsaw","E Berlin","F Nice","G [dead]","H Paris","I Moscow"]
        v = diplomacy_solve(list3in)
        self.assertEqual(v,list3out)

    def test_solve_4(self):
        list1in = ["A Madrid Hold","B Barcelona Move Madrid","C London Support B"]
        list1out = ["A [dead]","B Madrid","C London"]
        v = diplomacy_solve(list1in)
        self.assertEqual(v,list1out)

# ----
# main
# ----

if __name__ == "__main__":
    main()


""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
