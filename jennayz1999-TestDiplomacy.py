#!/usr/bin/env python3

# -------------------------------
# projects/diplomacy/TestDiplomacy.py
# -------------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import read_diplomacy, compute_diplomacy, output_diplomacy, sameItems, similarItems, solve_diplomacy

# -----------
# TestDiplomacy
# -----------

class TestDiplomacy (TestCase):

    def test_solve(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        solve_diplomacy(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support A\n")
        w = StringIO()
        solve_diplomacy(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC London\n")
    
    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        solve_diplomacy(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        solve_diplomacy(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")


# -----
# main
# -----

if __name__ == "__main__": # pragma: no cover 
    main()