from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve, diplomacy_reads, diplomacy_eval, diplomacy_print


class TestDiplomacy(TestCase):

    def test_solve_1(self):
        r = StringIO('A Venice Move Sydney\nB Mumbai Hold\nC Sydney Move Venice\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Sydney\nB Mumbai\nC Venice\n')

    
    def test_solve_2(self):
        r = StringIO('A Houston Hold\nB Orlando Move Houston\nC Sydney Hold\nD London Move Sydney\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalule(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

    def test_solve_3(self):
        r = StringIO('A Denver Move Houston\nB Houston Support D\nC NewYork Support B\nD Jacksonville Move NewYork\n')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A [dead]\nB [dead]\nC [dead]\nD [dead]\n')

if __name__ == "main":
    main()