#!/usr/bin/env python3

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import Army, City, diplomacy_read, diplomacy_eval, diplomacy_print, diplomacy_solve, diplomacy_build

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read
    # ----

    def test_read_1(self):
        s = "A Madrid Hold\nB Dallas Move Madrid\n"
        a = diplomacy_read(s)
        assert a, "test_read_1 did not return an answer"
        self.assertEqual(a[0][0],  'A')
        self.assertEqual(a[0][1], 'Madrid')
        self.assertEqual(a[0][2], 'Hold')
        self.assertEqual(a[1][0],  'B')
        self.assertEqual(a[1][1], 'Dallas')
        self.assertEqual(a[1][2], 'Move')
        self.assertEqual(a[1][3], 'Madrid')

    def test_read_2(self):
        with open("acceptanceTests/RunDiplomacy1.in", 'r') as f:
            a = diplomacy_read(f)
            self.assertEqual(a[0][0],  'A')
            self.assertEqual(a[0][1], 'London')
            self.assertEqual(a[0][2], 'Hold')
            self.assertEqual(a[1][0],  'B')
            self.assertEqual(a[1][1], 'Boston')
            self.assertEqual(a[1][2], 'Support')
            self.assertEqual(a[1][3], 'A')
            self.assertEqual(a[2][0], 'C')
            self.assertEqual(a[2][1], 'Houston')
            self.assertEqual(a[2][2], 'Move')
            self.assertEqual(a[2][3], 'Madrid')
            self.assertEqual(a[3][0], 'D')
            self.assertEqual(a[3][1], 'Austin')
            self.assertEqual(a[3][2], 'Move')
            self.assertEqual(a[3][3], 'Madrid')
            self.assertEqual(a[4][0], 'E')
            self.assertEqual(a[4][1], 'Dallas')
            self.assertEqual(a[4][2], 'Move')
            self.assertEqual(a[4][3], 'Boston')

    # ----
    # build
    # ----

    def test_build_1(self):
        armies = [['A', 'Madrid', 'Support', 'B'], ['B', 'London', 'Hold'], 
             ['C', 'Houston', 'Move', 'Madrid'], ['D', 'Austin', 'Support', 'B']]
        a, c = diplomacy_build(armies)
        assert a, "no armies were returned"
        assert c, "no cities were returned"
        # --- armies ---
        # army: A
        self.assertEqual(a[0].id, 'A')
        self.assertTrue(a[0].dead)
        self.assertEqual(str(type(a[0].supparmy)), "<class 'NoneType'>")
        self.assertEqual(a[0].supparmy, None)
        self.assertEqual(a[0].support, 0)
        self.assertEqual(str(type(a[0].city)), "<class 'Diplomacy.City'>")
        self.assertEqual(a[0].city.name, "Madrid")
        # army: B
        self.assertEqual(a[1].id, 'B')
        self.assertTrue(a[1].dead)
        self.assertEqual(a[1].support, 1)
        self.assertEqual(str(type(a[1].city)), "<class 'Diplomacy.City'>")
        self.assertEqual(a[1].city.name, "London")
        # army: C
        self.assertEqual(a[2].id, 'C')
        self.assertTrue(a[2].dead)
        self.assertEqual(a[2].support, 0)
        self.assertEqual(str(type(a[2].city)), "<class 'Diplomacy.City'>")
        self.assertEqual(a[2].city.name, "Madrid")
        # army: D
        self.assertEqual(a[3].id, 'D')
        self.assertTrue(a[3].dead)
        self.assertEqual(str(type(a[3].supparmy)), "<class 'Diplomacy.Army'>")
        self.assertEqual(a[3].supparmy.id, "B")
        self.assertEqual(a[3].support, 0)
        self.assertEqual(str(type(a[3].city)), "<class 'Diplomacy.City'>")
        self.assertEqual(a[3].city.name, "Austin")
        # --- cities ---
        self.assertEqual(c[0].name, "Madrid")
        self.assertEqual(c[0].armies, [a[0], a[2]])
        self.assertEqual(c[1].name, "London")
        self.assertEqual(c[2].name, "Austin")

    def test_build_2(self):
        armies = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']]
        a, c = diplomacy_build(armies)
        assert a, "no armies were returned"
        assert c, "no cities were returned"
        # --- armies ---
        # army: A
        self.assertEqual(a[0].id, 'A')
        self.assertTrue(a[0].dead)
        self.assertEqual(a[0].support, 0)
        self.assertEqual(str(type(a[0].city)), "<class 'Diplomacy.City'>")
        self.assertEqual(a[0].city.name, "Madrid")
        # army: B
        self.assertEqual(a[1].id, 'B')
        self.assertTrue(a[1].dead)
        self.assertEqual(a[1].support, 1)
        self.assertEqual(str(type(a[1].city)), "<class 'Diplomacy.City'>")
        self.assertEqual(a[1].city.name, "Madrid")
        # army: C
        self.assertEqual(a[2].id, 'C')
        self.assertTrue(a[2].dead)
        self.assertEqual(a[2].support, 0)
        self.assertEqual(str(type(a[2].city)), "<class 'Diplomacy.City'>")
        self.assertEqual(a[2].city.name, "London")


    
    # ----
    # eval
    # ----
    
    def test_eval_1(self):
        armies = [['A', 'Madrid', 'Support', 'B'], ['B', 'London', 'Hold'], 
             ['C', 'Houston', 'Move', 'Madrid'], ['D', 'Austin', 'Support', 'B']]
        a, c = diplomacy_build(armies) 
        output = diplomacy_eval(a, c)
        self.assertEqual(output, [['A', '[dead]'], ['B', 'London'], ['C', '[dead]'], ['D', 'Austin']])
    
    def test_eval_2(self):
        armies = [['A', 'Madrid', 'Hold'], ['B', 'Barcelona', 'Move', 'Madrid'], ['C', 'London', 'Support', 'B']]
        a, c = diplomacy_build(armies) 
        output = diplomacy_eval(a, c)
        self.assertEqual(output, [['A', '[dead]'], ['B', 'Madrid'], ['C', 'London']])

    # -----
    # print
    # -----

    # commented out test files for solve and print b/c they're not done yet
    def test_print_1(self):
        w = StringIO()
        diplomacy_print(w, [['A', 'Madrid'], ['B', '[dead]']])
        self.assertEqual(w.getvalue(), "A Madrid\nB [dead]\n")

    # -----
    # solve
    # -----

    # TODO: Error here (AttributeError: '_io.StringIO' object has no attribute 'splitlines')
    def test_solve_1(self):
        r = StringIO("A Paris Move Madrid\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        # makes sure extracted buffer wvalue w is equal to the StringIO
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    
    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\nF Barcelona Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        # makes sure extracted buffer wvalue w is equal to the StringIO
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD [dead]\nE Austin\nF [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        # makes sure extracted buffer wvalue w is equal to the StringIO
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC [dead]\nD Paris\n")

# ----
# main
# ----

if __name__ == "__main__": # pragma: no cover
    main()
