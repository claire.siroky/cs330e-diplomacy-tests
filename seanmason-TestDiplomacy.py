# TestDiplomacy.py

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve,diplomacy_format

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    '''
    solve
    '''
    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n")
    
    def test_solve_2(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB London Move Madrid\nC Austin Support B\nD Beijing Move Austin\n")        
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")
    
    '''
    format
    '''
    def test_format_1(self):
        list_input = [["A","Dallas","Move","Mexico","alive"],["C","Dallas","Hold","dead"],["B","Houston","n","n","alive"],["E","AUstin","n","n","alive"],["D","Dallas","n","n","dead"]]
        list_formatted = diplomacy_format(list_input)
        self.assertEqual(list_formatted,[['A', 'Dallas'], ['B', 'Houston'], ['C', '[dead]'], ['D', '[dead]'], ['E', 'AUstin']])
    
    def test_format_2(self):
        list_input = [["A","Dallas","Move","Mexico","alive"],["B","Houston","n","n","alive"],["C","Dallas","Hold","dead"],["D","Dallas","n","n","dead"],["E","AUstin","n","n","alive"]]
        list_formatted = diplomacy_format(list_input)
        self.assertEqual(list_formatted,[['A', 'Dallas'], ['B', 'Houston'], ['C', '[dead]'], ['D', '[dead]'], ['E', 'AUstin']])

    def test_format_3(self):
        list_input = []
        list_formatted = diplomacy_format(list_input)
        self.assertEqual(list_formatted,[])
# ----
# main
# ----


if __name__ == "__main__":
    main()
""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1


$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Diplomacy.py          12      0      2      0   100%
TestDiplomacy.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
        